<?php

namespace App\Repository;

use App\Entity\Vitamin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vitamin|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vitamin|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vitamin[]    findAll()
 * @method Vitamin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VitaminRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vitamin::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('v')
            ->where('v.something = :value')->setParameter('value', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
